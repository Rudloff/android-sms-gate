#!/bin/bash

set -e

export PATH=$PATH:$PWD/node_modules/.bin/

echo $PATH

MINIFY=$(which minify)
if [ -z "$MINIFY" ]; then
	sudo npm install -g minifier
fi
BROWSERIFY=$(which browserify)
if [ -z "$BROWSERIFY" ]; then
	sudo npm install -g browserify
fi

npm install
browserify index.js -o smsgate.js

minify jquery.viewbox.js
