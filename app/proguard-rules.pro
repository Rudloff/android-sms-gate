# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/axet/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontobfuscate

-dontwarn com.fsck.k9.mail.**
-dontwarn com.google.firebase.**
-dontwarn com.google.android.**

-keep class com.github.axet.smsgate.services.FirebaseService$Info {*;}
-keep class com.github.axet.smsgate.services.FirebaseService$Message {*;}
