package com.github.axet.smsgate.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.github.axet.smsgate.providers.SMS;

import java.util.Date;

/**
 * am start -n com.github.axet.smsgate/.activities.SendSMS -e phone +199988877766 -e msg "hello"
 * <p>
 * am start -n com.github.axet.smsgate/.activities.SendSMS -e phone 000100 -e msg "b"
 */
public class SendSMS extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = getIntent().getExtras();

        sendSMS(this, b.getString("phone"), b.getString("msg"), null, null);

        finish();
    }

    public static void sendSMS(Context context, String phone, String msg, Date date, String thread) {
        SMS.send(context, phone, msg);
    }
}
