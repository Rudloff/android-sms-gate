package com.github.axet.smsgate.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.widgets.OptimizationPreferenceCompat;
import com.github.axet.smsgate.app.MainApplication;
import com.github.axet.smsgate.app.ScheduleSMS;
import com.github.axet.smsgate.app.ScheduleTime;
import com.github.axet.smsgate.fragments.SchedulersFragment;
import com.github.axet.smsgate.providers.SMS;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TreeSet;

public class ScheduleService extends Service implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String TAG = ScheduleService.class.getSimpleName();

    // upcoming noticiation alarm action. triggers notification upcoming.
    public static final String REGISTER = ScheduleService.class.getCanonicalName() + ".REGISTER";
    public static final String ACTION_SMS = ScheduleService.class.getCanonicalName() + ".SMS";

    List<ScheduleSMS> items;

    OptimizationPreferenceCompat.ServiceReceiver receiver;

    public static void start(Context context) {
        Intent intent = new Intent(context, ScheduleService.class);
        intent.setAction(REGISTER);
        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        receiver = new OptimizationPreferenceCompat.ServiceReceiver(this, getClass(), MainApplication.PREF_OPTIMIZATION) {
            @Override
            public void check() {
            }

            @Override
            public void register() {
                super.register();
                OptimizationPreferenceCompat.setKillCheck(context, next, MainApplication.PREF_NEXT);
            }

            @Override
            public void unregister() {
                super.unregister();
                OptimizationPreferenceCompat.setKillCheck(context, 0, MainApplication.PREF_NEXT);
            }
        };

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        receiver.close();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        receiver.onStartCommand(intent, flags, startId);
        if (intent != null) {
            String action = intent.getAction();
            Log.d(TAG, "onStartCommand " + action);
            if (action != null) {
                if (action.equals(REGISTER)) {
                    MainApplication.load(this);
                    registerNextAlarm();
                }
                if (action.equals(ACTION_SMS)) {
                    long time = intent.getLongExtra("time", 0);
                    sms(time);
                }
            }
        } else {
            Log.d(TAG, "onStartCommand restart");
        }
        return super.onStartCommand(intent, flags, startId);
    }

    public void registerNextAlarm() {
        TreeSet<Long> events = new TreeSet<>();

        items = MainApplication.load(this);

        for (ScheduleSMS s : items) {
            if (s.next != 0 && s.enabled)
                events.add(s.next);
        }

        Calendar cur = Calendar.getInstance();

        Intent alarmIntent = new Intent(this, ScheduleService.class).setAction(ACTION_SMS);

        if (events.isEmpty()) {
            AlarmManager.cancel(this, alarmIntent);
        } else {
            long time = events.first();

            alarmIntent.putExtra("time", time);

            Log.d(TAG, "Current: " + ScheduleSMS.formatDate(cur.getTimeInMillis()) + " " + ScheduleSMS.formatTime(cur.getTimeInMillis()) + "; SetAlarm: " + ScheduleSMS.formatDate(time) + " " + ScheduleSMS.formatTime(time));

            AlarmManager.setExact(this, time, alarmIntent);
        }
    }

    public void sms(long time) {
        List<ScheduleSMS> delete = new ArrayList<>();
        for (ScheduleSMS s : items) {
            if (s.next == time && s.enabled) {
                if (s.phone.isEmpty())
                    continue;
                Log.d(TAG, "Send SMS: " + s.phone);
                if (s.sim == -1) {
                    if (s.hide)
                        SMS.send(s.phone, s.message);
                    else
                        SMS.send(this, s.phone, s.message);
                } else {
                    if (s.hide)
                        SMS.send(s.sim, s.phone, s.message);
                    else
                        SMS.send(this, s.sim, s.phone, s.message);
                }
                if (s.repeat == ScheduleTime.REPEAT_DELETE)
                    delete.add(s);
                else
                    s.fired();
            }
        }
        for (ScheduleSMS s : delete) {
            items.remove(s);
        }
        MainApplication.save(this, items);
        if (!delete.isEmpty())
            SchedulersFragment.reload(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        registerNextAlarm();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
