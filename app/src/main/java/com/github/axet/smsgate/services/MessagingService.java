package com.github.axet.smsgate.services;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.zegoggles.smssync.activity.SMSGateFragment;

public class MessagingService extends FirebaseMessagingService {
    public static final String TAG = MessagingService.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "FCM Message Id: " + remoteMessage.getMessageId());
        Log.d(TAG, "FCM Notification Message: " + remoteMessage.getNotification());
        Log.d(TAG, "FCM Data Message: " + remoteMessage.getData());

        String text = remoteMessage.getData().get("text");
        if (text == null)
            return;

        SMSGateFragment.checkPermissions(this);
        FirebaseService.command(this, text);
    }

    @Override
    public void onDeletedMessages() {
        Log.d(TAG, "onDeletedMessages");
        super.onDeletedMessages();
    }
}
