package com.github.axet.smsgate.services;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.preference.PreferenceManager;
import android.telephony.CellSignalStrength;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.github.axet.androidlibrary.crypto.Bitcoin;
import com.github.axet.androidlibrary.services.DeviceAdmin;
import com.github.axet.androidlibrary.services.WifiReceiver;
import com.github.axet.smsgate.R;
import com.github.axet.smsgate.activities.MainActivity;
import com.github.axet.smsgate.app.MainApplication;
import com.github.axet.smsgate.providers.SIM;
import com.github.axet.smsgate.providers.SMS;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.zegoggles.smssync.Consts;
import com.zegoggles.smssync.SmsConsts;
import com.zegoggles.smssync.mail.DataType;
import com.zegoggles.smssync.mail.PersonLookup;
import com.zegoggles.smssync.mail.PersonRecord;
import com.zegoggles.smssync.service.BackupCursors;
import com.zegoggles.smssync.service.BackupItemsFetcher;
import com.zegoggles.smssync.service.BackupQueryBuilder;
import com.zegoggles.smssync.service.BulkFetcher;
import com.zegoggles.smssync.service.ServiceBase;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import static com.zegoggles.smssync.Consts.SMS_PROVIDER;

@TargetApi(11)
public class FirebaseService extends Service implements FirebaseAuth.AuthStateListener, SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String TAG = FirebaseService.class.getSimpleName();

    public static final String INCOMING = FirebaseService.class.getCanonicalName() + ".INCOMING";
    public static final String TOKEN = FirebaseService.class.getCanonicalName() + ".TOKEN";
    public static final String COMMAND = FirebaseService.class.getCanonicalName() + ".COMMAND";
    public static final String OPEN = FirebaseService.class.getCanonicalName() + ".OPEN"; // open share incoming web link or share dialog
    public static final String COPY = FirebaseService.class.getCanonicalName() + ".COPY"; // copy share incoming click to clipboard
    public static final String SHARE = FirebaseService.class.getCanonicalName() + ".SHARE"; // share incoming text or files
    public static final String DELETE = FirebaseService.class.getCanonicalName() + ".DELETE";
    public static final String INTENT = FirebaseService.class.getCanonicalName() + ".INTENT";
    public static final String FIREBASE = FirebaseService.class.getCanonicalName() + ".FIREBASE";
    public static final String NOTIFICATION = FirebaseService.class.getCanonicalName() + ".NOTIFICATION";

    public static final int NETWORK_TYPE_LTE_CA = 19;

    public static int LOGIN_CHECK = 60 * 1000;

    static int count = 0;

    FirebaseDatabase db;
    FirebaseAuth mFirebaseAuth;
    DatabaseReference connected;
    ValueEventListener connectedList;
    DatabaseReference user;
    DatabaseReference messages;
    DatabaseReference shares;
    DatabaseReference notifications;
    DatabaseReference info;
    StorageReference storage;
    DatabaseReference uploads;
    Bitcoin keyPair;
    boolean authenticated;
    PersonLookup mPersonLookup;
    SIM sim;
    Handler handler = new Handler();
    FirebaseMessaging topic;
    String oldTopic;
    Runnable login; // waiting for authontificated event

    NotificationService.NotificationsMap<NotificationInfo> notifications_map = new NotificationService.NotificationsMap<>(handler);

    HashMap<String, Command> commands = new HashMap<>();

    BroadcastReceiver receiver;
    WifiReceiver wifiReciver;

    MyPhoneStateListener phone;

    {
        commands.put("reboot", new Command() {
            @Override
            public void run(JSONObject obj) {
                DeviceAdmin.reboot(FirebaseService.this);
            }
        });
        commands.put("refresh", new Command() {
            @Override
            public void run(JSONObject obj) {
                messagesExists(new Runnable() {
                    @Override
                    public void run() {
                        messages();
                    }
                });
            }
        });
        commands.put("send", new Command() {
            @Override
            public void run(JSONObject obj) {
                try {
                    int i = obj.optInt("sim", 0); // Web index default(0), sim1(1), sim2(2)
                    i -= 1; // sms index = default(-1), sim1(0), sim2(1)
                    if (i == -1) {
                        SMS.send(FirebaseService.this, obj.getString("phone"), obj.getString("message"));
                    } else {
                        SMS.send(FirebaseService.this, sim.getSimID(i), obj.getString("phone"), obj.getString("message"));
                    }
                    incoming();
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        commands.put("deleteall", new Command() {
            @Override
            public void run(JSONObject obj) {
                try {
                    deleteall(obj.getInt("thread"));
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        commands.put("share", new Command() {
            @Override
            public void run(JSONObject obj) {
                String uploads = obj.optString("uploads", null);
                JSONArray aa = obj.optJSONArray("uris");
                share(obj.optString("text"), uploads, toBundle(aa));
            }
        });
        commands.put("notificationcancel", new Command() {
            @Override
            public void run(JSONObject obj) {
                try {
                    if (Build.VERSION.SDK_INT >= 18) {
                        NotificationListener.cancel(FirebaseService.this, obj.getString("id"));
                    }
                    // force remove notification, if notification listener fails to do so. requires for API<18
                    Intent intent = new Intent();
                    intent.putExtra("action", NotificationService.REMOVE);
                    intent.putExtra("id", obj.getString("id"));
                    notification(intent);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        commands.put("wipe", new Command() {
            @Override
            public void run(JSONObject obj) {
                DeviceAdmin.wipe(FirebaseService.this);
            }
        });
        commands.put("lock", new Command() {
            @Override
            public void run(JSONObject obj) {
                DeviceAdmin.lock(FirebaseService.this);
            }
        });
    }

    class MyPhoneStateListener extends PhoneStateListener {
        int level;
        TelephonyManager tm;
        ConnectivityManager cm;
        PackageManager pm;
        boolean connected;
        int voice = TelephonyManager.NETWORK_TYPE_UNKNOWN;

        public MyPhoneStateListener() {
            tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            tm.listen(this, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS | PhoneStateListener.LISTEN_SERVICE_STATE);
            cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            pm = getPackageManager();
        }

        public void close() {
            tm.listen(this, PhoneStateListener.LISTEN_NONE);
        }

        public boolean isConnected() {
            return connected;
        }

        public boolean isTelephony() {
            return pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
        }

        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            if (Build.VERSION.SDK_INT >= 23) {
                level = signalStrength.getLevel();
            } else {
                if (signalStrength.isGsm()) {
                    level = getGsmLevel(signalStrength);
                } else {
                    level = getCdmaLevel(signalStrength);
                }
            }
        }

        @Override
        public void onServiceStateChanged(ServiceState serviceState) {
            super.onServiceStateChanged(serviceState);
            connected = serviceState.getState() == ServiceState.STATE_IN_SERVICE;
            Class k = serviceState.getClass();
            try {
                Method m = k.getMethod("getVoiceNetworkType");
                voice = (int) m.invoke(serviceState);
            } catch (Exception ignore) {
                try {
                    Method m = k.getMethod("getNetworkType"); // old api, networktype -> voicenetworktype
                    voice = (int) m.invoke(serviceState);
                } catch (Exception ignore2) {
                }
            }
        }
    }

    public static int getGsmLevel(SignalStrength signalStrength) {
        int level;
        // ASU ranges from 0 to 31 - TS 27.007 Sec 8.5
        // asu = 0 (-113dB or less) is very weak
        // signal, its better to show 0 bars to the user in such cases.
        // asu = 99 is a special case, where the signal strength is unknown.
        int asu = signalStrength.getGsmSignalStrength();
        if (asu <= 2 || asu == 99) level = CellSignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;
        else if (asu >= 12) level = CellSignalStrength.SIGNAL_STRENGTH_GREAT;
        else if (asu >= 8) level = CellSignalStrength.SIGNAL_STRENGTH_GOOD;
        else if (asu >= 5) level = CellSignalStrength.SIGNAL_STRENGTH_MODERATE;
        else level = CellSignalStrength.SIGNAL_STRENGTH_POOR;
        return level;
    }

    public static int getCdmaLevel(SignalStrength signalStrength) {
        final int cdmaDbm = signalStrength.getCdmaDbm();
        final int cdmaEcio = signalStrength.getCdmaEcio();
        int levelDbm;
        int levelEcio;

        if (cdmaDbm >= -75) levelDbm = CellSignalStrength.SIGNAL_STRENGTH_GREAT;
        else if (cdmaDbm >= -85) levelDbm = CellSignalStrength.SIGNAL_STRENGTH_GOOD;
        else if (cdmaDbm >= -95) levelDbm = CellSignalStrength.SIGNAL_STRENGTH_MODERATE;
        else if (cdmaDbm >= -100) levelDbm = CellSignalStrength.SIGNAL_STRENGTH_POOR;
        else levelDbm = CellSignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;

        // Ec/Io are in dB*10
        if (cdmaEcio >= -90) levelEcio = CellSignalStrength.SIGNAL_STRENGTH_GREAT;
        else if (cdmaEcio >= -110) levelEcio = CellSignalStrength.SIGNAL_STRENGTH_GOOD;
        else if (cdmaEcio >= -130) levelEcio = CellSignalStrength.SIGNAL_STRENGTH_MODERATE;
        else if (cdmaEcio >= -150) levelEcio = CellSignalStrength.SIGNAL_STRENGTH_POOR;
        else levelEcio = CellSignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN;

        int level = (levelDbm < levelEcio) ? levelDbm : levelEcio;
        return level;
    }

    public static String getNetworkTypeName(int type) {
        switch (type) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "GPRS";
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "EDGE";
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "UMTS";
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "HSDPA";
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "HSUPA";
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "HSPA";
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "CDMA";
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return "CDMA - EvDo rev. 0";
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return "CDMA - EvDo rev. A";
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return "CDMA - EvDo rev. B";
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return "CDMA - 1xRTT";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "LTE";
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return "CDMA - eHRPD";
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "iDEN";
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "HSPA+";
            case TelephonyManager.NETWORK_TYPE_GSM:
                return "GSM";
            case TelephonyManager.NETWORK_TYPE_TD_SCDMA:
                return "TD_SCDMA";
            case TelephonyManager.NETWORK_TYPE_IWLAN:
                return "IWLAN";
            case NETWORK_TYPE_LTE_CA:
                return "LTE_CA";
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                return "UNKNOWN";
            default:
                return "UNKNOWN(" + type + ")";
        }
    }

    public static class NotificationInfo extends NotificationService.NotificationInfo {
        Intent intent;

        public NotificationInfo(String id) {
            super(id);
        }
    }

    public interface Command {
        void run(JSONObject json);
    }

    public static class Info extends Message {
        private String version;

        public Info() {
        }

        public Info(String v, String m) {
            super(m);
            version = v;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }
    }

    public static class Message {
        private String id;
        private Long date;
        private String text;

        public Message() {
        }

        public Message(String text) {
            this.date = System.currentTimeMillis();
            this.text = text;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public void setDate(Long date) {
            this.date = date;
        }

        public Long getDate() {
            return date;
        }
    }

    public static class SMSMessage {
        String id;
        long sent;
        String type; // IN/OUT
        String thread; // thread id
        String threadPhone; // phone
        String threadName; // person Full Name
        String message;

        public String toJSON() {
            try {
                JSONObject j = new JSONObject();
                j.put("id", id);
                j.put("sent", sent);
                j.put("type", type);
                j.put("thread", thread);
                j.put("threadPhone", threadPhone);
                j.put("threadName", threadName);
                j.put("message", message);
                return j.toString();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public long getSent() {
            return sent;
        }

        public void setSent(long sent) {
            this.sent = sent;
        }

        public String getThread() {
            return thread;
        }

        public String getThreadName() {
            return threadName;
        }

        public String getThreadPhone() {
            return threadPhone;
        }

        public void setThread(String thread) {
            this.thread = thread;
        }

        public void setThreadName(String threadName) {
            this.threadName = threadName;
        }

        public void setThreadPhone(String threadPhone) {
            this.threadPhone = threadPhone;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    public static class QueryBuilder extends BackupQueryBuilder {
        SharedPreferences prefs;

        public QueryBuilder(Context context) {
            super(context);
            prefs = PreferenceManager.getDefaultSharedPreferences(context);
        }

        @Override
        public long getLastMMS() {
            return prefs.getLong(MainApplication.MMS_LAST, 0);
        }

        public void setLastMMS(long l) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putLong(MainApplication.MMS_LAST, l);
            edit.commit();
        }

        @Override
        public long getLastSMS() {
            return prefs.getLong(MainApplication.SMS_LAST, 0);
        }

        public void setLastSMS(long l) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putLong(MainApplication.SMS_LAST, l);
            edit.commit();
        }

        @Override
        public Query getQueryForSMS(int max) {
            return new Query(Consts.SMS_PROVIDER,
                    null,
                    String.format(Locale.ENGLISH,
                            "%s > ?",
                            SmsConsts.DATE).trim(),
                    new String[]{
                            String.valueOf(getLastSMS())
                    },
                    max);
        }

        @Override
        public long getTypeSMS() {
            return SmsConsts.MESSAGE_TYPE_ALL;
        }
    }

    public static ArrayList<Bundle> toBundle(JSONArray aa) {
        ArrayList<Bundle> bb = null;
        try {
            if (aa != null && aa.length() > 0) {
                bb = new ArrayList<>();
                for (int i = 0; i < aa.length(); i++) {
                    JSONObject o = aa.getJSONObject(i);
                    Bundle b = new Bundle();
                    Iterator<String> ss = o.keys();
                    while (ss.hasNext()) {
                        String key = ss.next();
                        Object v = o.get(key);
                        if (v instanceof String)
                            b.putString(key, (String) v);
                        if (v instanceof Long)
                            b.putLong(key, (Long) v);
                        if (v instanceof Integer)
                            b.putInt(key, (Integer) v);
                    }
                    bb.add(b);
                }
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return bb;
    }

    // update firebase status text
    public static void firebase(Context context) {
        Intent intent = new Intent();
        intent.setAction(FIREBASE);
        context.sendBroadcast(intent);
    }

    public static void start(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(MainApplication.PREF_FIREBASE, true);
        edit.commit();

        firebase(context);

        Intent intent = new Intent(context, FirebaseService.class);
        context.startService(intent);
    }

    public static void notification(Context context, String action, String pkg, String id, Notification n) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (!prefs.getBoolean(MainApplication.PREF_NOTIFICATION_LISTENER, false)) {
            return;
        }

        // ignore low priority notifications
        if (Build.VERSION.SDK_INT >= 16) {
            if (n.priority <= Notification.PRIORITY_LOW)
                return;
        }

        String applicationName = getApplicationName(context, pkg);

        Intent intent = new Intent(FirebaseService.NOTIFICATION);
        intent.putExtra("id", id);
        intent.putExtra("action", action);

        String title;
        String text;
        String details = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            title = toString(n.extras.get("android.title"));
            if (title == null || title.isEmpty())
                title = applicationName;

            text = toString(n.extras.get("android.text")); // can be spannable
            if (text == null || text.isEmpty()) {
                text = toString(n.tickerText); // can be null
            }
            details = toString(n.extras.get("android.bigText")); // class android.text.SpannableString
        } else {
            title = applicationName;
            text = toString(n.tickerText); // can be null
        }

        intent.putExtra("title", title);
        intent.putExtra("text", text);
        intent.putExtra("details", details);

        context.sendBroadcast(intent);
    }

    public static String getApplicationName(Context context, String pkg) {
        final PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo info;
            info = pm.getApplicationInfo(pkg, 0);
            return getApplicationName(context, info);
        } catch (final PackageManager.NameNotFoundException e) {
            return pkg;
        }
    }

    public static String getApplicationName(Context context, ApplicationInfo info) {
        final PackageManager pm = context.getPackageManager();
        String n = toString(pm.getApplicationLabel(info));
        if (n == null || n.isEmpty())
            n = toString(info.loadLabel(pm));
        if (n == null || n.isEmpty())
            n = info.packageName;
        return n;
    }

    public static String toString(Object o) {
        if (o == null)
            return null;
        return o.toString();
    }

    public static void intent(Context context, Bundle args) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(MainApplication.PREF_FIREBASE, true);
        edit.commit();

        firebase(context);

        Intent intent = new Intent(context, FirebaseService.class);
        intent.setAction(INTENT);
        intent.putExtra("args", args);
        context.startService(intent);
    }

    public static void startIfEnabled(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (prefs.getBoolean(MainApplication.PREF_FIREBASE, false))
            start(context);
    }

    public static void incoming(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (prefs.getBoolean(MainApplication.PREF_FIREBASE, false)) {
            Intent intent = new Intent(context, FirebaseService.class);
            intent.setAction(INCOMING);
            context.startService(intent);
        }
    }

    public static void token(Context context) {
        if (isConnected(context)) {
            Intent intent = new Intent(context, FirebaseService.class);
            intent.setAction(TOKEN);
            context.startService(intent);
        }
    }

    public static void reset(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putLong(MainApplication.MMS_LAST, 0);
        edit.putLong(MainApplication.SMS_LAST, 0);
        edit.commit();
    }

    public static void command(Context context, String enc) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(MainApplication.PREF_FIREBASE, true);
        edit.commit();

        Intent intent = new Intent(context, FirebaseService.class);
        intent.setAction(COMMAND);
        intent.putExtra("text", enc);
        context.startService(intent);
    }

    public static void stop(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(MainApplication.PREF_FIREBASE, false);
        edit.commit();
        firebase(context);

        Intent intent = new Intent(context, FirebaseService.class);
        context.stopService(intent);

        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseAuth.signOut();
    }

    public static boolean isConnected(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(MainApplication.PREF_FIREBASE, false);
    }

    public static boolean connected() {
        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        return mFirebaseAuth.getCurrentUser() != null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        phone = new MyPhoneStateListener();

        create();

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent == null)
                    return;
                String a = intent.getAction();
                if (a == null)
                    return;
                if (a.equals(NOTIFICATION)) {
                    notification(intent);
                }
            }
        };
        IntentFilter ff = new IntentFilter();
        ff.addAction(NOTIFICATION);
        registerReceiver(receiver, ff);

        wifiReciver = new WifiReceiver(this) {
            @Override
            public void resume() {
                FirebaseService.this.resume();
            }

            @Override
            public void pause() {
                FirebaseService.this.pause();
            }

            @Override
            public boolean getWifi() {
                return prefs.getBoolean(MainApplication.PREF_WIFI, false);
            }
        };
        wifiReciver.create();
    }

    void create() {
        mPersonLookup = new PersonLookup(getContentResolver());
        sim = new SIM(this);
        topic = FirebaseMessaging.getInstance();

        keyPair = MainApplication.getApp(this).getKeyPair();

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseAuth.addAuthStateListener(this);

        db = FirebaseDatabase.getInstance();

        connected = db.getReference(".info/connected");
        connectedList = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    Log.d(TAG, "connected ref");
                } else {
                    Log.d(TAG, "not connected ref");
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.d(TAG, "connected cancelled", error.toException());
            }
        };
        connected.addValueEventListener(connectedList);

        Log.d(TAG, "connect to " + keyPair.getAddress());

        user = db.getReference().child("/users/" + keyPair.getAddress());
        shares = user.child("shares");
        notifications = user.child("notifications");
        messages = user.child("messages");
        info = user.child("info");
        storage = FirebaseStorage.getInstance().getReference().child("/users/" + keyPair.getAddress());
        uploads = user.child("uploads");

        // load current uploads after restart / reconnect
        uploads.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d(TAG, "uploads " + dataSnapshot);
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    Message m = d.getValue(Message.class);
                    try {
                        String text = keyPair.decrypt(m.getText());
                        JSONObject obj = new JSONObject(text);
                        String uploads = d.getKey();
                        JSONArray aa = obj.getJSONArray("uris");
                        share(obj.getString("text"), uploads, toBundle(aa));
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        login();
    }

    void close() {
        if (oldTopic != null) {
            topic.unsubscribeFromTopic(oldTopic);
            oldTopic = null;
        }
        if (mFirebaseAuth != null) {
            mFirebaseAuth.signOut();
            mFirebaseAuth.removeAuthStateListener(this);
            mFirebaseAuth = null;
        }
        if (connected != null) {
            connected.removeEventListener(connectedList);
            connected = null;
        }
        handler.removeCallbacks(login);
        login = null;
    }

    void reload() {
        close();
        create();
    }

    void process(String enc) {
        try {
            String text = keyPair.decrypt(enc);
            JSONObject json = new JSONObject(text);
            String command = json.getString("command");
            Log.d(TAG, command);
            Command run = commands.get(command);
            if (run == null)
                throw new RuntimeException("unknown command: " + command);
            run.run(json);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    void login() {
        Log.d(TAG, "login");
        mFirebaseAuth.signOut();
        mFirebaseAuth.signInWithEmailAndPassword("anonymous@anonymous.com", "anonymous").addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                Log.d(TAG, "login success " + authResult);
                handler.removeCallbacks(login);
                login = null;
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "Failed to login", e);
            }
        });
        login = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "login failed");
                reload();
            }
        };
        handler.postDelayed(login, LOGIN_CHECK); // old phones failed to login, timeout 1 minute, then relogin
        if (oldTopic != null) {
            topic.unsubscribeFromTopic(oldTopic);
        }
        oldTopic = keyPair.getAddress();
        topic.subscribeToTopic(oldTopic);
    }

    public static boolean isPause(Context context) {
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean wifi = prefs.getBoolean(MainApplication.PREF_WIFI, false);
        if (wifi) {
            return !WifiReceiver.isConnectedWifi(context);
        } else {
            return false;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        if (intent != null) {
            String a = intent.getAction();
            if (a != null) {
                if (a.equals(INCOMING)) {
                    incoming();
                    info();
                }
                if (a.equals(TOKEN)) {
                    reload();
                }
                if (a.equals(INTENT)) {
                    try {
                        Bundle args = intent.getBundleExtra("args");
                        final JSONObject json = new JSONObject();
                        json.put("text", args.getString("text"));
                        byte[] preview = args.getByteArray("preview");
                        if (preview != null) {
                            json.put("preview", Base64.encodeToString(preview, Base64.DEFAULT));
                        }
                        final DatabaseReference ref = shares.push();
                        Object uri = args.get("uri");
                        if (uri != null) {
                            final JSONArray jj = new JSONArray();
                            final ArrayList<Bundle> bb = (ArrayList<Bundle>) uri;
                            for (Bundle b : bb) {
                                final JSONObject item = new JSONObject();
                                item.put("mimetype", b.getString("mimetype"));
                                item.put("size", b.getLong("size"));
                                item.put("filename", b.getString("filename"));
                                try {
                                    Uri u = (Uri) b.get("uri");
                                    byte[] buf = IOUtils.toByteArray(getContentResolver().openInputStream(u));
                                    buf = keyPair.encrypt(buf);
                                    storage.child(ref.getKey()).child(UUID.randomUUID().toString()).putBytes(buf).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            Uri url = taskSnapshot.getDownloadUrl();
                                            try {
                                                item.put("uri", url.toString());
                                                jj.put(item);
                                                if (jj.length() == bb.size()) {
                                                    json.put("uri", jj);
                                                    ref.setValue(new Message(keyPair.encrypt(json.toString())));
                                                }
                                            } catch (JSONException e) {
                                                throw new RuntimeException(e);
                                            }
                                        }
                                    });
                                } catch (IOException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        } else {
                            ref.setValue(new Message(keyPair.encrypt(json.toString())));
                        }
                        Toast.makeText(this, "Shared!", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }
                if (a.equals(COMMAND)) {
                    try {
                        process(intent.getStringExtra("text"));
                        info();
                    } catch (RuntimeException e) {
                        Log.e(TAG, "process", e);
                    }
                }
                if (a.equals(DELETE)) {
                    String up = intent.getStringExtra("uploads");
                    ArrayList<Bundle> uris = intent.getParcelableArrayListExtra("uris");
                    if (uris != null) {
                        for (Bundle b : uris) {
                            String uri = b.getString("uri");
                            FirebaseStorage.getInstance().getReferenceFromUrl(uri).delete().addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    Log.e(TAG, "delete", exception);
                                }
                            });
                        }
                    }
                    uploads.child(up).removeValue();
                }
                if (a.equals(COPY)) {
                    String text = intent.getStringExtra("text");
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("text", text);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(this, "Link copied", Toast.LENGTH_SHORT).show();
                }
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (phone != null) {
            phone.close();
            phone = null;
        }

        close();

        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }

        if (wifiReciver != null) {
            unregisterReceiver(wifiReciver);
            wifiReciver = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        Log.d(TAG, "onAuthStateChanged");
        FirebaseUser mFirebaseUser = firebaseAuth.getCurrentUser();
        authenticated(mFirebaseUser);
    }

    public void authenticated(FirebaseUser mFirebaseUser) {
        if (mFirebaseUser == null) {
            if (authenticated) {
                authenticated = false;
                firebase(this);
            }
            return;
        }
        if (authenticated)
            return;
        mFirebaseUser.getToken(true).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "getToken failure");
                reload();
            }
        }).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
            @Override
            public void onSuccess(GetTokenResult getTokenResult) {
                Log.d(TAG, "getToken " + getTokenResult.getToken());
            }
        });
        mFirebaseUser.reload().addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "reload() failure");
                reload();
            }
        }).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "reload() ok");
            }
        });
        authenticated = true;
        firebase(this);
    }

    public static Map<String, String> getMessageMap(Cursor cursor) {
        final String[] columns = cursor.getColumnNames();
        final Map<String, String> msgMap = new HashMap<String, String>(columns.length);
        for (String column : columns) {
            String value;
            try {
                final int index = cursor.getColumnIndex(column);
                if (index != -1) {
                    value = cursor.getString(index);
                } else {
                    continue;
                }
            } catch (SQLiteException ignored) {
                // this can happen in case of BLOBS in the DB
                // column type checking is API level >= 11
                value = "[BLOB]";
            }
            msgMap.put(column, value);
        }
        return msgMap;
    }

    public void incoming() {
        final Runnable run = new Runnable() {
            int count = 0;

            @Override
            public void run() {
                count++;
                if (count > 10)
                    return;
                if (messages() == 0) { // we expect message. not yet here? retry
                    handler.postDelayed(this, 1000);
                }
            }
        };
        messagesExists(run);
    }

    public void messagesExists(final Runnable run) {
        user.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild("messages")) {
                    QueryBuilder b = new QueryBuilder(FirebaseService.this);
                    b.setLastSMS(0);
                }
                run.run();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    public int messages() {
        final QueryBuilder b = new QueryBuilder(this);
        BackupItemsFetcher fetcher = new BackupItemsFetcher(this, getContentResolver(), b);
        BackupCursors cursors = new BulkFetcher(fetcher).fetch(EnumSet.of(DataType.SMS), -1);

        int count = 0;
        while (cursors.hasNext()) {
            BackupCursors.CursorAndType cursor = cursors.next();
            count++;
            if (cursors.filter())
                continue;
            Map<String, String> map = getMessageMap(cursor.cursor);
            SMSMessage sms = messageFromMapSms(map);
            messages.child(sms.getId()).setValue(new Message(keyPair.encrypt(sms.toJSON())));
            if (cursor.type == DataType.SMS) {
                b.setLastSMS(sms.sent);
            }
        }
        cursors.close();

        return count;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
        if (key.equals(MainApplication.PUB)) {
            reload();
        }
        if (key.equals(MainApplication.PREF_WIFI)) {
            if (prefs.getBoolean(MainApplication.PREF_FIREBASE, false) && !isPause(this)) {
                resume();
            } else {
                pause();
            }
        }
    }

    public SMSMessage messageFromMapSms(Map<String, String> msgMap) {
        SMSMessage sms = new SMSMessage();

        sms.id = msgMap.get(SmsConsts.ID);
        sms.message = msgMap.get(SmsConsts.BODY);

        final int messageType = toInt(msgMap.get(SmsConsts.TYPE));

        sms.thread = msgMap.get(SmsConsts.THREAD_ID);

        final String address = msgMap.get(SmsConsts.ADDRESS);
        if (TextUtils.isEmpty(address)) {
            sms.threadPhone = "--";
            sms.threadName = "--";
        } else {
            PersonRecord record = mPersonLookup.lookupPerson(address);
            sms.threadPhone = record.getNumber();
            sms.threadName = record.getName();
        }

        if (sms.thread == null || sms.thread.isEmpty())
            sms.thread = sms.threadPhone;

        if (SmsConsts.MESSAGE_TYPE_INBOX == messageType) {
            // Received message
            sms.type = "IN";
        } else {
            // Sent message
            sms.type = "OUT";
        }

        long sentDate;
        try {
            sentDate = Long.valueOf(msgMap.get(SmsConsts.DATE));
        } catch (NumberFormatException n) {
            Log.e(TAG, "error parsing date", n);
            sentDate = System.currentTimeMillis();
        }
        sms.sent = sentDate;

        return sms;
    }

    private static int toInt(String s) {
        try {
            return Integer.valueOf(s);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    void updateAllThreads() {
        // thread dates + states might be wrong, we need to force a full update
        // unfortunately there's no direct way to do that in the SDK, but passing a
        // negative conversation id to delete should to the trick
        getContentResolver().delete(Uri.parse("content://sms/conversations/-1"), null, null);
    }

    void deleteall(final int thread) {
        ContentResolver r = getContentResolver();
        Cursor c = r.query(SMS_PROVIDER, new String[]{SmsConsts.ID, SmsConsts.THREAD_ID}, SmsConsts.THREAD_ID + " = ?", new String[]{Integer.toString(thread)}, null);
        if (c != null && c.moveToFirst()) {
            do {
                String id = c.getString(0);
                long threadId = c.getLong(1);
                if (threadId == thread) {
                    String ref = "content://sms/" + id;
                    Log.d(TAG, "Delete SMS" + ref);
                    r.delete(Uri.parse(ref), null, null);
                    messages.child(id).removeValue();
                }
            } while (c.moveToNext());
            c.close();
            return; // do not delete thread manually
        }
        messages.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    Message m = d.getValue(Message.class);
                    try {
                        String text = keyPair.decrypt(m.getText());
                        JSONObject obj = new JSONObject(text);
                        String id = d.getKey();
                        if (obj.getInt("thread") == thread)
                            messages.child(id).removeValue();
                    } catch (JSONException e) {
                        throw new RuntimeException(e);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.toString());
            }
        });
    }

    public void share(String text, String uploads, ArrayList<Bundle> uris) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        count++;

        // open browser or share incoming dialog
        Intent browserIntent;
        if (uris == null) {
            Uri uri = Uri.parse(text);
            browserIntent = new Intent(Intent.ACTION_VIEW, uri);
            browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } else {
            browserIntent = new Intent(this, MainActivity.class);
            browserIntent.setAction(MainActivity.SHARE);
            browserIntent.putExtra("text", text);
            browserIntent.putParcelableArrayListExtra("uris", uris);
            browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        PendingIntent button = PendingIntent.getActivity(this, count,
                browserIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent copy = PendingIntent.getService(this, count,
                new Intent(this, FirebaseService.class).setAction(COPY).putExtra("text", text),
                PendingIntent.FLAG_UPDATE_CURRENT);

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, text);
        sendIntent.setType("text/plain");
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent share = PendingIntent.getActivity(this, count,
                sendIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // when notification removed, delete uploads from server
        PendingIntent delete = PendingIntent.getService(this, count,
                new Intent(this, FirebaseService.class).setAction(DELETE).putExtra("uploads", uploads).putParcelableArrayListExtra("uris", uris),
                PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews view = new RemoteViews(getPackageName(), R.layout.share_notification);
        view.setOnClickPendingIntent(R.id.notification_button, button);
        view.setOnClickPendingIntent(R.id.notification_copy, copy);
        view.setOnClickPendingIntent(R.id.notification_share, share);

        try {
            URL url = new URL(text);
        } catch (MalformedURLException e) {
            view.setViewVisibility(R.id.notification_button, View.GONE);
        }

        if (uris != null) {
            view.setTextViewText(R.id.notification_subject, "Shared Files");
            view.setOnClickPendingIntent(R.id.notification_base, button);
            view.setViewVisibility(R.id.notification_copy, View.GONE);
            view.setViewVisibility(R.id.notification_share, View.GONE);

            String com = ", ";
            if (!text.isEmpty())
                text += com;
            for (Bundle b : uris) {
                text += b.getString("filename") + com;
            }
            if (text.endsWith(com))
                text = text.substring(0, text.lastIndexOf(com));
        }
        view.setTextViewText(R.id.notification_text, text);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setTicker(text)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(text)
                .setSmallIcon(R.drawable.ic_sms)
                .setContent(view);

        if (uploads != null)
            builder.setDeleteIntent(delete);

        if (Build.VERSION.SDK_INT < 11)
            builder.setContentIntent(button);

        if (Build.VERSION.SDK_INT >= 21)
            builder.setVisibility(Notification.VISIBILITY_PUBLIC);

        Notification n;

        if (Build.VERSION.SDK_INT >= 16)
            n = builder.build();
        else {
            n = builder.getNotification();
        }

        notificationManager.notify(ServiceBase.BACKUP_FIRE + count, n);
    }

    void notification(Intent intent) {
        String id = intent.getStringExtra("id");
        String action = intent.getStringExtra("action");

        if (action.equals(NotificationService.REMOVE)) {
            notifications_map.remove(id);
            notifications.child(id).removeValue();
            return;
        }

        String title = intent.getStringExtra("title");
        String text = intent.getStringExtra("text");
        String details = intent.getStringExtra("details");

        int code = (title + " " + text + " " + details).hashCode();

        NotificationInfo cc = notifications_map.get(id);
        if (cc == null)
            cc = new NotificationInfo(id);

        if (notifications_map.duplicate(cc, code))
            return;

        cc.intent = intent;

        long now = System.currentTimeMillis();

        if (notifications_map.delayed(cc, now)) {
            final NotificationInfo info = cc;
            notifications_map.delay(cc, new Runnable() {
                @Override
                public void run() {
                    notification(info.intent);
                }
            });
            return;
        }

        notifications_map.put(cc, code, now);

        try {
            JSONObject json = new JSONObject();
            json.put("title", title);
            json.put("text", text);
            json.put("details", details);
            notifications.child(id).setValue(new Message(keyPair.encrypt(json.toString())));
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    void info() {
        try {
            final JSONObject json = new JSONObject();
            json.put("uptime", Long.toString(SystemClock.elapsedRealtime()));
            json.put("os", Build.BRAND + " " + android.os.Build.VERSION.RELEASE);
            json.put("sim", sim.getCount());
            json.put("battery", getBatteryLevel());
            if (phone.isTelephony())
                json.put("signal", getSignalLevel());
            String version = "";
            try {
                PackageManager pm = getPackageManager();
                PackageInfo pInfo = pm.getPackageInfo(getPackageName(), 0);
                version = "v" + pInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                throw new RuntimeException(e);
            }
            info.setValue(new Info(version, keyPair.encrypt(json.toString()))).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d(TAG, "info updated");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d(TAG, "info failed update");
                }
            });
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    String getBatteryLevel() {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(null, ifilter);
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;

        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        String s = "";
        if (isCharging) {
            switch (chargePlug) {
                case BatteryManager.BATTERY_PLUGGED_USB:
                    s += "USB: ";
                    break;
                case BatteryManager.BATTERY_PLUGGED_AC:
                    s += "AC: ";
                    break;
                case BatteryManager.BATTERY_PLUGGED_WIRELESS:
                    s += "WL: ";
                    break;
                default:
                    s += "OTG: "; // unknown. OTG cable for example.
            }
        } else {
            s += "BAT: ";
        }
        s += (level * 100 / scale) + "%";
        return s;
    }

    String getSignalLevel() {
        int type;
        if (phone.voice != 0)
            type = phone.voice;
        else if (Build.VERSION.SDK_INT >= 24 && checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
            type = phone.tm.getVoiceNetworkType();
        else
            type = phone.tm.getNetworkType();
        if (phone.isConnected())
            return getNetworkTypeName(type) + ": " + (phone.level * 100 / 4) + "%";
        else
            return "NO SIGNAL";
    }

    void pause() {
        Log.d(TAG, "pause");
        db.goOffline();
        handler.removeCallbacks(login);
    }

    void resume() {
        Log.d(TAG, "resume");
        db.goOnline();
        if (login != null)
            handler.postDelayed(login, LOGIN_CHECK);
    }
}
