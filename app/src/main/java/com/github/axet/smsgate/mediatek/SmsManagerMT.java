package com.github.axet.smsgate.mediatek;

import android.app.PendingIntent;

import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * https://labs.mediatek.com/site/znch/developer_tools/mediatek_android/android_sdk/api_references/mediatek-sdk3/reference/com/mediatek/telephony/SmsManagerEx.gsp
 * <p>
 * http://git.huayusoft.com/tomsu/AP7200_MDK-kernel/blob/master/mediatek/frameworks/api/1.txt
 */
public class SmsManagerMT {
    Class sms_class;
    Object m;

    public SmsManagerMT() {
        try {
            sms_class = Class.forName("com.mediatek.telephony.SmsManagerEx");
            Method m = sms_class.getMethod("getDefault");
            this.m = m.invoke(null);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public ArrayList<String> divideMessage(String s) {
        try {
            Method m = sms_class.getMethod("divideMessage", String.class);
            return (ArrayList<String>) m.invoke(this.m, s);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static SmsManagerMT getDefault() {
        return new SmsManagerMT();
    }

    public void sendDataMessage(java.lang.String s, java.lang.String ss, short sss, byte[] n, android.app.PendingIntent p, android.app.PendingIntent pp, int i) {
    }

    public void sendDataMessage(java.lang.String s, java.lang.String ss, short sss, short ssss, byte[] n, android.app.PendingIntent p, android.app.PendingIntent pp, int i) {
    }

    public void sendMultipartTextMessage(String phone, String ss, ArrayList<String> parts, ArrayList<PendingIntent> p, ArrayList<PendingIntent> pp, int sim) {
        try {
            Method m = sms_class.getMethod("sendMultipartTextMessage", String.class, String.class, new ArrayList<String>().getClass(), new ArrayList<PendingIntent>().getClass(), new ArrayList<PendingIntent>().getClass(), int.class);
            m.invoke(this.m, phone, ss, parts, p, pp, sim);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void sendMultipartTextMessageWithExtraParams(java.lang.String s, java.lang.String ss, java.util.ArrayList<java.lang.String> sss, android.os.Bundle b, java.util.ArrayList<android.app.PendingIntent> nn, java.util.ArrayList<android.app.PendingIntent> p, int i) {
    }

    public void sendTextMessage(String phone, String ss, String text, PendingIntent p, PendingIntent pp, int sim) {
        try {
            Method m = sms_class.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class, int.class);
            m.invoke(this.m, phone, ss, text, p, pp, sim);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void sendTextMessageWithExtraParams(java.lang.String s, java.lang.String ss, java.lang.String sss, android.os.Bundle b, android.app.PendingIntent p, android.app.PendingIntent pp, int i) {
    }
}
