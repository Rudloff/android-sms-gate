package com.zegoggles.smssync.service.state;

public enum SmsSyncState {
    INITIAL,
    CALC,
    LOGIN,
    BACKUP,
    REPLYSMS,
    RESTORE,
    ERROR,
    CANCELED_BACKUP,
    CANCELED_RESTORE,
    FINISHED_BACKUP,
    FINISHED_REPLYSMS,
    FINISHED_RESTORE,
    UPDATING_THREADS
}
