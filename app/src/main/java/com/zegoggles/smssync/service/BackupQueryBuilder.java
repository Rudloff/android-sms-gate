package com.zegoggles.smssync.service;

import android.content.Context;
import android.net.Uri;

import com.zegoggles.smssync.Consts;
import com.zegoggles.smssync.MmsConsts;
import com.zegoggles.smssync.SmsConsts;
import com.zegoggles.smssync.mail.DataType;

import org.jetbrains.annotations.Nullable;

import java.util.Locale;

import static com.zegoggles.smssync.mail.DataType.MMS;
import static com.zegoggles.smssync.mail.DataType.SMS;

public class BackupQueryBuilder {
    private final Context context;

    public BackupQueryBuilder(Context context) {
        this.context = context;
    }

    public static class Query {
        final Uri uri;
        final String[] projection;
        final String selection;
        final String[] selectionArgs;
        final String sortOrder;

        public Query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
            this.uri = uri;
            this.projection = projection;
            this.selection = selection;
            this.selectionArgs = selectionArgs;
            this.sortOrder = sortOrder;
        }

        public Query(Uri uri, String[] projection, String selection, String[] selectionArgs, int max) {
            this(uri, projection, selection, selectionArgs,
                    max > 0 ? SmsConsts.DATE + " LIMIT " + max : SmsConsts.DATE);
        }
    }

    public
    @Nullable
    Query buildQueryForDataType(DataType type, int max) {
        switch (type) {
            case SMS:
                return getQueryForSMS(max);
            case MMS:
                return getQueryForMMS(max);
            default:
                return null;
        }
    }

    public
    @Nullable
    Query buildMostRecentQueryForDataType(DataType type) {
        switch (type) {
            case MMS:
                return new Query(
                        Consts.MMS_PROVIDER,
                        new String[]{MmsConsts.DATE},
                        null,
                        null,
                        MmsConsts.DATE + " DESC LIMIT 1");
            case SMS:
                return new Query(
                        Consts.SMS_PROVIDER,
                        new String[]{SmsConsts.DATE},
                        SmsConsts.TYPE + " <> ?",
                        new String[]{String.valueOf(SmsConsts.MESSAGE_TYPE_DRAFT)},
                        SmsConsts.DATE + " DESC LIMIT 1");
            default:
                return null;
        }
    }

    public Query getQueryForSMS(int max) {
        return new Query(Consts.SMS_PROVIDER,
                null,
                String.format(Locale.ENGLISH,
                        "%s > ? and %s == ?",
                        SmsConsts.DATE,
                        SmsConsts.TYPE).trim(),
                new String[]{
                        String.valueOf(getLastSMS()),
                        String.valueOf(getTypeSMS()),
                },
                max);
    }

    public Query getQueryForMMS(int max) {
        return new Query(
                Consts.MMS_PROVIDER,
                null,
                String.format(Locale.ENGLISH, "%s > ? AND %s <> ? %s",
                        SmsConsts.DATE,
                        MmsConsts.TYPE,
                        "").trim(),
                new String[]{
                        String.valueOf(getLastMMS()),
                        MmsConsts.DELIVERY_REPORT
                },
                max);
    }

    public long getLastSMS() {
        return SMS.getMaxSyncedDate(context);
    }

    public long getLastMMS() {
        long maxSynced = MMS.getMaxSyncedDate(context);
        if (maxSynced > 0) {
            // NB: max synced date is stored in seconds since epoch in database
            maxSynced = (long) (maxSynced / 1000d);
        }
        return maxSynced;
    }

    public long getTypeSMS() {
        return SmsConsts.MESSAGE_TYPE_INBOX;
    }
}
