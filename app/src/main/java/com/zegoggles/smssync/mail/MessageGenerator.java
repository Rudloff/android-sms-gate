package com.zegoggles.smssync.mail;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.internet.MimeMultipart;
import com.fsck.k9.mail.internet.TextBody;
import com.zegoggles.smssync.Consts;
import com.zegoggles.smssync.MmsConsts;
import com.zegoggles.smssync.SmsConsts;
import com.zegoggles.smssync.preferences.AddressStyle;
import com.zegoggles.smssync.preferences.CallLogTypes;
import com.zegoggles.smssync.preferences.Preferences;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

import static com.fsck.k9.mail.internet.MimeMessageHelper.setBody;
import static com.zegoggles.smssync.App.LOCAL_LOGV;
import static com.zegoggles.smssync.App.TAG;
import static com.zegoggles.smssync.Consts.MMS_PART;

class MessageGenerator {
    private final Context mContext;
    private final HeaderGenerator mHeaderGenerator;
    private final Address mUserAddress;
    private final PersonLookup mPersonLookup;
    private final boolean mPrefix;
    private final CallFormatter mCallFormatter;
    private final AddressStyle mAddressStyle;
    private final MmsSupport mMmsSupport;
    private final CallLogTypes mCallLogTypes;

    public MessageGenerator(Context context,
                            Address userAddress,
                            AddressStyle addressStyle,
                            HeaderGenerator headerGenerator,
                            PersonLookup personLookup,
                            boolean mailSubjectPrefix,
                            MmsSupport mmsSupport) {
        mHeaderGenerator = headerGenerator;
        mUserAddress = userAddress;
        mAddressStyle = addressStyle;
        mContext = context;
        mPersonLookup = personLookup;
        mPrefix = mailSubjectPrefix;
        mCallFormatter = new CallFormatter(mContext.getResources());
        mMmsSupport = mmsSupport;
        mCallLogTypes = CallLogTypes.getCallLogType(new Preferences(context));
    }

    public
    @Nullable
    Message messageForDataType(Map<String, String> msgMap, DataType dataType) throws MessagingException {
        switch (dataType) {
            case SMS:
                return messageFromMapSms(msgMap);
            case MMS:
                return messageFromMapMms(msgMap);
            default:
                return null;
        }
    }

    private
    @Nullable
    Message messageFromMapSms(Map<String, String> msgMap) throws MessagingException {
        final String address = msgMap.get(SmsConsts.ADDRESS);
        if (TextUtils.isEmpty(address)) return null;

        PersonRecord record = mPersonLookup.lookupPerson(address);

        final Message msg = new MimeMessage();
        msg.setSubject(getSubject(DataType.SMS, record));
        setBody(msg, new TextBody(msgMap.get(SmsConsts.BODY)));

        final int messageType = toInt(msgMap.get(SmsConsts.TYPE));

        // encode send / to address for easy reply function.
        // now to replay sms you just need reply email (it will be send to your inbox)
        Address addr = new Address(mUserAddress);
        addr.setPersonal(record.getNumber());

        if (SmsConsts.MESSAGE_TYPE_INBOX == messageType) {
            // Received message
            msg.setFrom(addr);
            msg.setRecipient(Message.RecipientType.TO, mUserAddress);
        } else {
            // Sent message
            msg.setRecipient(Message.RecipientType.TO, addr);
            msg.setFrom(mUserAddress);
        }

        Date sentDate;
        try {
            sentDate = new Date(Long.valueOf(msgMap.get(SmsConsts.DATE)));
        } catch (NumberFormatException n) {
            Log.e(TAG, "error parsing date", n);
            sentDate = new Date();
        }
        mHeaderGenerator.setHeaders(msg, msgMap, DataType.SMS, address, record, sentDate, messageType);
        //msg.setUsing7bitTransport();
        return msg;
    }

    private
    @Nullable
    Message messageFromMapMms(Map<String, String> msgMap) throws MessagingException {
        if (LOCAL_LOGV) Log.v(TAG, "messageFromMapMms(" + msgMap + ")");

        final Uri mmsUri = Uri.withAppendedPath(Consts.MMS_PROVIDER, msgMap.get(MmsConsts.ID));
        MmsSupport.MmsDetails details = mMmsSupport.getDetails(mmsUri, mAddressStyle);

        if (details.isEmpty()) {
            Log.w(TAG, "no recipients found");
            return null;
        }

        final Message msg = new MimeMessage();
        msg.setSubject(getSubject(DataType.MMS, details.getRecipient()));

        if (details.inbound) {
            // msg_box == MmsConsts.MESSAGE_BOX_INBOX does not work
            msg.setFrom(details.getRecipientAddress());
            msg.setRecipient(Message.RecipientType.TO, mUserAddress);
        } else {
            msg.setRecipients(Message.RecipientType.TO, details.getAddresses());
            msg.setFrom(mUserAddress);
        }

        Date sentDate;
        try {
            sentDate = new Date(1000 * Long.valueOf(msgMap.get(MmsConsts.DATE)));
        } catch (NumberFormatException n) {
            Log.e(TAG, "error parsing date", n);
            sentDate = new Date();
        }
        final int msg_box = toInt(msgMap.get("msg_box"));
        mHeaderGenerator.setHeaders(msg, msgMap, DataType.MMS, details.address, details.getRecipient(), sentDate, msg_box);
        MimeMultipart body = MimeMultipart.newInstance();

        for (BodyPart p : mMmsSupport.getMMSBodyParts(Uri.withAppendedPath(mmsUri, MMS_PART))) {
            body.addBodyPart(p);
        }

        setBody(msg, body);
        //msg.setUsing7bitTransport();
        return msg;
    }

    private String getSubject(@NotNull DataType type, @NotNull PersonRecord record) {
        return mPrefix ?
                String.format(Locale.ENGLISH, "[%s] %s", type.getFolder(mContext), record.getName()) :
                mContext.getString(type.withField, record.getName());
    }

    private static int toInt(String s) {
        try {
            return Integer.valueOf(s);
        } catch (NumberFormatException e) {
            return -1;
        }
    }
}
